# HAM Logger

Logging Software for Amateur Radio "Specialists" (because every HAM is an expert).

It will be entirely web-based and will be able to run off-line

## Features

 - Net Control
   - *Live* updates
   - FCC Database Lookups with Preferred names
   - Multiple Modes: Swap, ARES Training, ARES Tactical, Club, Contest
   - Check-in without taking radio time
   - Scheduled nets can be subscribed to to go onto calendar
 - Station Log
   - Events: Updating antennas and radios, lightning or other possible damage
   - Contacts: 
     - Radio Contacts - those you want to do QSL cards (Paper, eQSL, LotW, internal)
     - TODO contacts - people you want to contact
     - Known contacts - those you talk to on the local repeater but you don't want to do a QSO
   - Net check-ins
   - General Notes
 - Repeater
   - New repeaters on RepeaterBook will be imported
   - Reviews
   - More info than RepeaterBook for digital Modes
   - Favorites
   - Route Finder (Google Maps or OpenStreetMaps)
 - Web Application (Can be installed on any computer, tablet, or smartphone)
 - Profile
   - Call sign with history (based on FRN)
   - Your contact history with the call sign
   - Home repeaters (how to find them)
   - Privacy options
   - DMR
 - Other helpful/quick info (that can be brought up any time, even while doing a net)
   - Band Plan and Simplex Frequencies
   - Phonetics, Q codes, telegraph codes, prosigns, etc.
   - ICS reference
 - Clubs (with management of them)
   - Calendar for Antenna parties, Club meetings, Field days, Tailgaters, Hamfests etc.
 - Forms and Voting
 - Equipment Inventory (since it sometimes goes with QSL's)
 - Possible Hardware Integrations
   - Radio control
   - Transmit updates over packet radio
   - Other serial controled tools (like a rotator)
   - Digital/CW encode/decode
 - Software Integrations
  - WinLink
  - EchoLink or AllStar
## Related Software

- [NetLogger](https://www.netlogger.org)
- [Amateur Radio Net Control Manager](https://net-control.us)
- [N3FJP](http://www.n3fjp.com)
- DXKeeper
- Winlog 32
- Ham Radio Deluxe
- QRZ.com (for profiles and qsl's)
- RepeaterBook.com (for repeaters)
- Logger Plus
