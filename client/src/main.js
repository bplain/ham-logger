import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import './index.css'

import VueFeather from 'vue-feather';

const app = createApp(App);
app.use(router)
app.use(store)
app.use(router)
app.component('feather', VueFeather)
app.mount('#app')
