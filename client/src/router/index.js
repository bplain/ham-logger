import { createRouter, createWebHashHistory } from 'vue-router';
import Index from '../views/Index.vue';

import Club from './club';
import Help from './help';
import Log from './log';
import Net from './net';
import Profile from './profile';
import Repeater from './repeater';

const baseRoutes = [
  {
    path: '/',
    name: 'Home',
    component: Index,
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
];

const routes = baseRoutes.concat(Club, Help, Log, Net, Profile, Repeater);

const router = createRouter({
  // using # while using gitlab pages for hosting
  history: createWebHashHistory(),
  routes
})

export default router
