import Net from '../views/Net/Index.vue';
import Controller from '../views/Net/Controller.vue';
import Manager from '../views/Net/Manager.vue';
import Logger from '../views/Net/Logger.vue';
import View from '../views/Net/View.vue';
import History from '../views/Net/History.vue';

export default [
    {
        path: '/net',
        name: 'net',
        component: Net,
        children: [
            {
                path: 'control',
                name: 'net-control',
                component: Controller,
            },
            {
                path: 'manage',
                name: 'net-manager',
                component: Manager,
            },
            {
                path: 'log',
                name: 'net-log',
                component: Logger,
            },
            {
                path: 'view',
                name: 'net-view',
                component: View,
            },
            {
                path: 'history',
                name: 'net-history',
                component: History,
            },
        ]
    }
]