import Profile from '../views/Profile/Index.vue';

export default [
    {
        path: '/profile',
        name: 'profile',
        component: Profile,
        children: [

        ]
    }
]