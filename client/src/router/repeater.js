import Repeater from '../views/Repeater/Index.vue';

export default [
    {
        path: '/repeater',
        name: 'repeater',
        component: Repeater,
        children: [

        ]
    }
]