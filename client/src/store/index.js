import { createStore } from 'vuex'

export default createStore({
  state: {
    netLogs: {}, // Active Net Logs by ID. Stored for preset amount of time locally before deletion.
    netCache: {}, // Cache of accessable/applicable nets for users.
    token: '',
    tokenTimestamp: '',
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
