<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Amateurs;
use App\Models\Club;

class UploadFccDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:fccdb {dir}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Uploads FCC Database from dat files in current directory.';

    protected $max_parameters = 999; // SQLite maximum

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $connection = config('database.default');
        $driver = config("database.connections.{$connection}.driver");
        if($driver == 'mysql' | $driver == 'pgsql') $this->max_parameters = 35565;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Open files from given directory
        $cd = $this->argument('dir');
        if(is_null($cd)) $cd = './';

        if(\DB::table('fcc_amateurs')->count() > 0){
            echo("This command is only for initializing the FCC Database!");
            return 1;
        }

        \DB::disableQueryLog();
        \DB::connection()->unsetEventDispatcher();

        $this->loadFile($cd.'AM.dat','fcc_amateurs','Amateurs',[
            'unique_system_identifier',
            'uls_file_num',
            'ebf_number',
            'callsign',
            'operator_class',
            'group_code',
            'region_code',
            'trustee_callsign',
            'trustee_indicator',
            'physician_certification',
            've_signature',
            'systematic_callsign_change',
            'vanity_callsign_change',
            'vanity_relationship',
            'previous_callsign',
            'previous_operator_class',
            'trustee_name',
        ]);

        $this->loadFile($cd.'CO.dat','fcc_comments','Comments',[
            'unique_system_identifier',
            'uls_file_num',
            'callsign',
            'comment_date',
            'description',
            'status_code',
            'status_date',
        ]);

        $this->loadFile($cd.'EN.dat', 'fcc_entities', 'Entities', [
            'unique_system_identifier',
            'uls_file_num',
            'ebf_number',
            'callsign',
            'entity_type',
            'licensee_id',
            'entity_name',
            'first_name',
            'mi',
            'last_name',
            'suffix',
            'phone',
            'fax',
            'email',
            'street_address',
            'city',
            'state',
            'zip_code',
            'po_box',
            'attention_line',
            'sgin',
            'frn',
            'applicant_type_code',
            'applicant_type_other',
            'status_code',
            'status_date',
            'lic_category_code',
            'linked_license_id',
            'linked_callsign',
        ]);
        $this->loadFile($cd.'HD.dat', 'fcc_headers', 'Header Entries', [
            'unique_system_identifier',
            'uls_file_number',
            'ebf_number',
            'call_sign',
            'license_status',
            'radio_service_code',
            'grant_date',
            'expired_date',
            'cancellation_date',
            'eligibility_rule_num',
            'applicant_type_code_reserved',
            'alien',
            'alien_government',
            'alien_corporation',
            'alien_officer',
            'alien_control',
            'revoked',
            'convicted',
            'adjudged',
            'involved_reserved',
            'common_carrier',
            'non_common_carrier',
            'private_comm',
            'fixed',
            'mobile',
            'radiolocation',
            'satellite',
            'developmental_or_sta',
            'interconnected_service',
            'certifier_first_name',
            'certifier_mi',
            'certifier_last_name',
            'certifier_suffix',
            'certifier_title',
            'gender',
            'african_american',
            'native_american',
            'hawaiian',
            'asian',
            'white',
            'ethnicity',
            'effective_date',
            'last_action_date',
            'auction_id',
            'reg_stat_broad_serv',
            'band_manager',
            'type_serv_broad_serv',
            'alien_ruling',
            'licensee_name_change',
            'whitespace_ind',
            'additional_cert_choice',
            'additional_cert_answer',
            'discontinuation_ind',
            'regulatory_compliance_ind',
        ]);
        $this->loadFile($cd.'HS.dat', 'fcc_histories', 'History Entries', [
            'unique_system_identifier',
            'uls_file_number',
            'callsign',
            'log_date',
            'code',
        ]);
        $this->loadFile($cd.'LA.dat', 'fcc_license_attachments', 'License Attachements', [
            'unique_system_identifier',
            'callsign',
            'attachment_code',
            'attachment_desc',
            'attachment_date',
            'attachment_filename',
            'action_performed',
        ]);
        $this->loadFile($cd.'SC.dat', 'fcc_special_conditions', 'Special Conditions', [
            'unique_system_identifier',
            'uls_file_number',
            'ebf_number',
            'callsign',
            'special_condition_type',
            'special_condition_code',
            'status_code',
            'status_date',
        ]);
        $this->loadFile($cd.'SF.dat', 'fcc_free_form_special_conditions', 'FF Special Conditions', [
            'unique_system_identifier',
            'uls_file_number',
            'ebf_number',
            'callsign',
            'lic_freeform_cond_type',
            'unique_lic_freeform_id',
            'sequence_number',
            'lic_freeform_condition',
            'status_code',
            'status_date',
        ]);


        $this->processAmateurs();
        $this->processClubs();
        return 0;
    }

    private function loadFile($filename,$table,$name,$columns){
        echo("Loading $name");
        array_unshift($columns,'type');
        $handle = fopen($filename,'r');
        $rows = [];
        $count = 0;
        $linecount = 0;
        $column_count = count($columns);
        $chunk_size = floor($this->max_parameters / $column_count) - 1;
        echo(" in chunks of $chunk_size\n");
        while(!feof($handle)){
            fgets($handle);
            $linecount++;
        }
        rewind($handle);
        while(($data = fgetcsv($handle,0,'|')) !== false){
            $datalength = count($data);
            if($column_count < $datalength){
                echo("\r$name #". ++$count . " column count incorrect: expected $column_count, got $datalength\n");
                echo("Counted $count/$linecount $name [" . str_repeat('=',$bars) . str_repeat(' ',30-$bars) . "] $percent%");
                continue;
            } elseif($column_count > $datalength){
                // Newlines are not escaped! Need to find the next line.
                while($column_count > $datalength){
                    $partial_data = fgetcsv($handle,0,'|');
                    if(count($partial_data) > 1){
                        end($data);
                        $data[key($data)] .= "\n" . array_shift($partial_data);
                        $data = array_merge($data, $partial_data);
                    }
                    $datalength = count($data);
                }
            }
            if($column_count != $datalength){
                echo("\r$name #". ++$count . " column count still incorrect: expected $column_count, got $datalength\n");
                echo("Counted $count/$linecount $name [" . str_repeat('=',$bars) . str_repeat(' ',30-$bars) . "] $percent%");
                continue;
            }
            // convert empty strings to null
            $data = array_map(function($item){
                if($item == '') return null;
                return $item;
            }, $data);
            $data = array_combine($columns,$data);
            unset($data['type']);
            $rows[] = $data;
            $count++;
            if(count($rows) > $chunk_size){
                $percent = (double)($count/$linecount);
                $bars = floor($percent*30);
                $percent = number_format($percent * 100, 0);
                echo("\rCounted $count/$linecount $name [" . str_repeat('=',$bars) . str_repeat(' ',30-$bars) . "] $percent%");
                \DB::table($table)->insert($rows);
                $rows = [];
            }
        }
        if(count($rows) > 0){
            \DB::table($table)->insert($rows);
            $rows = [];
        }
        echo("\rCounted $count $name.                                             \n");
        fclose($handle);
    }

    private function processAmateurs(){
        // 1) start with getting all active applications from HD
        \DB::table('fcc_headers')
            ->join('fcc_entities','fcc_headers.unique_system_identifier','=','fcc_entities.unique_system_identifier')
            ->whereNull('fcc_headers.expired_date')
            ->whereNotNull('fcc_headers.frn')
            ->where('fcc_entities.type','I')
            ->chunk(50, function($headers){
                foreach($headers as $header){
                    $history = \DB::table('fcc_headers')->select('grant_date')->where('frn',$header->frn)->order('grant_date')->limit(1)->value('grant_date');
                    $amateur = new Amateur;
                    $amateur->first_name = $header->first_name;
                    $amateur->last_name = $header->last_name;
                    $amateur->name = $header->entity_name;
                    $amateur->called_by = $header->first_name;
                    $amateur->grant_date = $history;
                    $amateur->frn = $header->frn;
                    $amateur->class = $header->class;
                    $amateur->street_address = $header->street_address;
                    $amateur->city = $header->city;
                    $amateur->state = $header->state;
                    $amatuer->zip_code = $header->zip_code;
                    $amateur->po_box = $header->po_box;
                    $amateur->attention_line = $header->attention_line;
                    $amateur->country = 'USA';
                    $amateur->save();
                }
            });
    }

    private function processClubs(){
        \DB::table('fcc_headers')
            ->join('fcc_entities','fcc_headers.unique_system_identifier','=','fcc_entities.unique_system_identifier')
            ->whereNull('fcc_headers.expired_date')
            ->whereNotNull('fcc_headers.frn')
            ->where('fcc_entities.type','B')
            ->chunk(50, function($headers){
                foreach($headers as $header){
                    $history = \DB::table('fcc_headers')->select('grant_date')->where('frn',$header->frn)->order('grant_date')->limit(1)->value('grant_date');
                    $club = new Club;
                    $club->name = $header->entity_name;
                    $club->grant_date = $history;
                    $club->frn = $header->frn;
                    $club->class = $header->class;
                    $club->street_address = $header->street_address;
                    $club->city = $header->city;
                    $club->state = $header->state;
                    $club->zip_code = $header->zip_code;
                    $club->po_box = $header->po_box;
                    $club->attention_line = $header->attention_line;
                    $club->country = 'USA';
                    $trustee = Amateur::where('callsign',$header->trustee_callsign)->first();
                    if($trustee){
                        $club->trustee_id = $trustee->id;
                    }
                    $club->trustee_name = $header->trustee_name;
                    $club->trustee_callsign = $header->trustee_callsign;
                    $club->save();
                }
            });
    }
}
