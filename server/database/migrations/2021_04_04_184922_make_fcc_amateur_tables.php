<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeFccAmateurTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Amateur Radio related tables
        // AM = amateur
        Schema::create('fcc_amateurs', function (Blueprint $table) {
            $table->bigInteger('unique_system_identifier');
            $table->char('uls_file_num',14)->nullable();
            $table->string('ebf_number',30)->nullable();
            $table->string('callsign',10)->nullable();
            $table->char('operator_class',1)->nullable();
            $table->char('group_code',1)->nullable();
            $table->tinyInteger('region_code')->nullable();
            $table->string('trustee_callsign',10)->nullable();
            $table->char('trustee_indicator',1)->nullable();
            $table->char('physician_certification',1)->nullable();
            $table->char('ve_signature',1)->nullable();
            $table->char('systematic_callsign_change',1)->nullable();
            $table->char('vanity_callsign_change',1)->nullable();
            $table->char('vanity_relationship',12)->nullable();
            $table->string('previous_callsign',10)->nullable();
            $table->char('previous_operator_class',1)->nullable();
            $table->string('trustee_name',50)->nullable();
        });
        //CO = comment
        Schema::create('fcc_comments', function (Blueprint $table) {
            $table->bigInteger('unique_system_identifier');
            $table->char('uls_file_num',14)->nullable();
            $table->string('callsign',10)->nullable();
            $table->char('comment_date',10)->nullable();
            $table->string('description')->nullable();
            $table->char('status_code',1)->nullable();
            $table->timestamp('status_date')->nullable();
        });
        // EN = entity
        Schema::create('fcc_entities', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('unique_system_identifier');
            $table->char('uls_file_num',14)->nullable();
            $table->string('ebf_number',30)->nullable();
            $table->string('callsign',10)->nullable();
            $table->char('entity_type',2)->nullable();
            $table->char('licensee_id',9)->nullable();
            $table->string('entity_name',200)->nullable();
            $table->string('first_name',20)->nullable();
            $table->char('mi',1)->nullable();
            $table->string('last_name',20)->nullable();
            $table->char('suffix',3)->nullable();
            $table->char('phone',10)->nullable();
            $table->char('fax',10)->nullable();
            $table->string('email',50)->nullable();
            $table->string('street_address',60)->nullable();
            $table->string('city',20)->nullable();
            $table->char('state',2)->nullable();
            $table->char('zip_code',9)->nullable();
            $table->string('po_box',20)->nullable();
            $table->string('attention_line',35)->nullable();
            $table->char('sgin',3)->nullable();
            $table->bigInteger('frn')->nullable();
            $table->char('applicant_type_code',1)->nullable();
            $table->char('applicant_type_other',40)->nullable();
            $table->char('status_code',1)->nullable();
            $table->timestamp('status_date')->nullable();
            $table->char('lic_category_code',1)->nullable();
            $table->bigInteger('linked_license_id')->nullable();
            $table->string('linked_callsign',10)->nullable();
        });
        // HD = Application / License Header
        Schema::create('fcc_headers', function (Blueprint $table) {
            $table->bigInteger('unique_system_identifier');
            $table->char('uls_file_number',14)->nullable();
            $table->string('ebf_number',30)->nullable();
            $table->char('call_sign',10)->nullable();
            $table->char('license_status',1)->nullable();
            $table->char('radio_service_code',2)->nullable();
            $table->char('grant_date',10)->nullable();
            $table->char('expired_date',10)->nullable();
            $table->char('cancellation_date',10)->nullable();
            $table->char('eligibility_rule_num',10)->nullable();
            $table->char('applicant_type_code_reserved',1)->nullable();
            $table->char('alien',1)->nullable();
            $table->char('alien_government',1)->nullable();
            $table->char('alien_corporation',1)->nullable();
            $table->char('alien_officer',1)->nullable();
            $table->char('alien_control',1)->nullable();
            $table->char('revoked',1)->nullable();
            $table->char('convicted',1)->nullable();
            $table->char('adjudged',1)->nullable();
            $table->char('involved_reserved',1)->nullable();
            $table->char('common_carrier',1)->nullable();
            $table->char('non_common_carrier',1)->nullable();
            $table->char('private_comm',1)->nullable();
            $table->char('fixed',1)->nullable();
            $table->char('mobile',1)->nullable();
            $table->char('radiolocation',1)->nullable();
            $table->char('satellite',1)->nullable();
            $table->char('developmental_or_sta',1)->nullable();
            $table->char('interconnected_service',1)->nullable();
            $table->string('certifier_first_name',20)->nullable();
            $table->char('certifier_mi',1)->nullable();
            $table->string('certifier_last_name',20)->nullable();
            $table->char('certifier_suffix',3)->nullable();
            $table->char('certifier_title',40)->nullable();
            $table->char('gender',1)->nullable();
            $table->char('african_american',1)->nullable();
            $table->char('native_american',1)->nullable();
            $table->char('hawaiian',1)->nullable();
            $table->char('asian',1)->nullable();
            $table->char('white',1)->nullable();
            $table->char('ethnicity',1)->nullable();
            $table->char('effective_date',10)->nullable();
            $table->char('last_action_date',10)->nullable();
            $table->integer('auction_id')->nullable();
            $table->char('reg_stat_broad_serv',1)->nullable();
            $table->char('band_manager',1)->nullable();
            $table->char('type_serv_broad_serv',1)->nullable();
            $table->char('alien_ruling',1)->nullable();
            $table->char('licensee_name_change',1)->nullable();
            $table->char('whitespace_ind',1)->nullable();
            $table->char('additional_cert_choice',1)->nullable();
            $table->char('additional_cert_answer',1)->nullable();
            $table->char('discontinuation_ind',1)->nullable();
            $table->char('regulatory_compliance_ind',1)->nullable();
        });
        // HS = History
        Schema::create('fcc_histories', function (Blueprint $table) {
            $table->bigInteger('unique_system_identifier');
            $table->char('uls_file_number',14)->nullable();
            $table->string('callsign',10)->nullable();
            $table->char('log_date',10)->nullable();
            $table->char('code',6)->nullable();
        });
        // LA = License Attachment
        Schema::create('fcc_license_attachments', function (Blueprint $table) {
            $table->bigInteger('unique_system_identifier');
            $table->string('callsign',10)->nullable();
            $table->char('attachment_code',1)->nullable();
            $table->string('attachment_desc',60)->nullable();
            $table->char('attachment_date',10)->nullable();
            $table->string('attachment_filename',60)->nullable();
            $table->char('action_performed',1)->nullable();
        });
        // SC = Special Condition
        Schema::create('fcc_special_conditions', function (Blueprint $table) {
            $table->char('record_type',2)->nullable();
            $table->bigInteger('unique_system_identifier');
            $table->char('uls_file_number',14)->nullable();
            $table->string('ebf_number',30)->nullable();
            $table->string('callsign',10)->nullable();
            $table->char('special_condition_type',1)->nullable();
            $table->integer('special_condition_code');
            $table->char('status_code',1)->nullable();
            $table->timestamp('status_date')->nullable();
        });
        // SF = License Free Form Special Condition
        Schema::create('fcc_free_form_special_conditions', function (Blueprint $table) {
            $table->char('record_type',2)->nullable();
            $table->bigInteger('unique_system_identifier');
            $table->char('uls_file_number',14)->nullable();
            $table->string('ebf_number',30)->nullable();
            $table->string('callsign',10)->nullable();
            $table->char('lic_freeform_cond_type',1)->nullable();
            $table->bigInteger('unique_lic_freeform_id')->nullable();
            $table->integer('sequence_number')->nullable();
            $table->string('lic_freeform_condition',255)->nullable();
            $table->char('status_code',1)->nullable();
            $table->timestamp('status_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
