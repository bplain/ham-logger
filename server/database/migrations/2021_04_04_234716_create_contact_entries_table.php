<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_entries', function (Blueprint $table) {
            $table->id();
            $table->foreignId('log_entry_id');
            $table->string('callsign',10)->nullable();
            $table->timestampTz('contact_start');
            $table->timestampTz('contact_end')->nullable();
            $table->foreignId('repeater_id')->nullable();
            $table->string('mode',20);
            $table->decimal('tx_frequency',6,4)->nullable();
            $table->decimal('rx_frequency',6,4)->nullable();
            $table->integer('rst_sent')->nullable();
            $table->integer('rst_receive')->nullable();
            $table->integer('power')->nullable();
            $table->foreignId('contest_id');
            $table->string('contest_arrl_section')->nullable();
            $table->string('contest_class')->nullable();
            $table->string('contest_info_sent')->nullable();
            $table->bigInteger('contest_sent_serial')->nullable();
            $table->string('contest_info_received')->nullable();
            $table->bigInteger('contest_received_serial')->nullable();
            $table->text('comments');
            $table->foreignId('propagation_id'); //id?
            $table->float('antenna_azumith')->nullable();
            $table->float('antenna_elevation')->nullable();
            $table->foreignId('antenna_path_id')->nullable();
            $table->integer('solar_k')->nullable();
            $table->integer('solar_a')->nullable();
            $table->integer('solar_sfi')->nullable();
            $table->foreignId('satellite_id')->nullable();
            $table->string('satellite_mode',20)->nullable();
            // todo: receiver info
            // todo: sender info
            // todo: qsl info (routes, card sent/rcv, eQSL, LoTW)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_entries');
    }
}
