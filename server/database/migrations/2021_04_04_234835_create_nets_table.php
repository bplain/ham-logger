<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // nets are scheduled events
        Schema::create('nets', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('primary_callsign',10);
            // location
            $table->point('location')->nullable();
            // simplex
            $table->decimal('frequency',6,4)->nullable();
            $table->string('mode',5)->nullable();
            // repeater
            $table->foreignId('repeater_id')->nullable();
            // schedule, repeat
            $table->string('schedule')->nullable();
            // type
            $table->foreignId('net_type');
            $table->foreignId('club_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nets');
    }
}
