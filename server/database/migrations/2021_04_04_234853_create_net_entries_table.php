<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNetEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('net_entries', function (Blueprint $table) {
            $table->id();
            // net log id
            $table->foreignId('net_log_id');
            // call
            $table->string('callsign',10)->nullable();
            // comment
            $table->text('comment');
            // in/out, no traffic, mobile, announce
            $table->timestamp('checkin')->nullable();
            $table->timestamp('checkout')->nullable();
            $table->char('priority',1)->nullable();
            $table->string('traffic_target_callsign',10)->nullable();
            $table->text('traffic')->nullable();
            // checkout
            // what is needed for emergency net? priority, last seen
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('net_entries');
    }
}
