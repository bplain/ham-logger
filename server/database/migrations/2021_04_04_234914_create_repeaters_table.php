<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepeatersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repeaters', function (Blueprint $table) {
            $table->id();
            // call
            $table->string('callsign',10);
            // location
            $table->point('location');
            $table->integer('power');
            // opn/closed
            $table->boolean('open');
            $table->boolean('listed')->default(true);
            $table->string('purpose');
            // rules
            // type
            
            $table->string('mode',20);
            // General frequencies
            $table->decimal('tx_frequency',6,4)->nullable();
            $table->decimal('rx_frequency',6,4)->nullable();
            // Analog tones
            $table->decimal('tx_tone',6,2)->nullable();
            $table->decimal('rx_tone',6,2)->nullable();
            //dmr color, timeslot uses, talkgroups json, brand
            $table->integer('dmr_color')->nullable();
            $table->string('dmr_ts1')->nullable();
            $table->string('dmr_ts2')->nullable();
            // echolink
            $table->integer('echolink_id')->nullable();
            $table->string('timezone',32)->nullable(); // php timezone
            $table->point('coordinates')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repeaters');
    }
}
