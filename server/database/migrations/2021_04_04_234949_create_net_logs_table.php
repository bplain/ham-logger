<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNetLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('net_logs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('net_id');
            $table->string('ncs_callsign',10);
            $table->string('logger_callsign',10);
            $table->timestampTz('start');
            $table->timestampTz('end')->nullable();
            $table->foreignId('next_log_id')->nullable();
            $table->text('notes')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('net_logs');
    }
}
