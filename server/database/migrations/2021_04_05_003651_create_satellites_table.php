<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSatellitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('satellites', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            // keplerian elements
            $table->timestampTz('epoch_time'); // T0
            $table->double('inclination');     // I0
            $table->double('ascending_node');  // O0
            $table->double('eccentricity');    // W0
            $table->double('perigee');         // E0
            $table->double('mean_speed');      // N0
            $table->double('mean_anomoly');    // M0  angle from the perigee
            $table->double('drag')->nullable();// N1
            $table->double('revolution')->nullable();
            $table->point('attitude')->nullable();
            // frequencies and modes
            $table->jsonb('modes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('satellites');
    }
}
