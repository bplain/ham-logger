<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRadiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('radios', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            // model -> mfg
            $table->string('model');
            $table->string('manufacturer');
            $table->decimal('cost',6,2);
            $table->date('release_date');
            $table->boolean('fm')->default('false');
            $table->boolean('am')->default('false');
            $table->boolean('dmr')->default('false');
            $table->boolean('tnc')->default('false');
            $table->boolean('bluetooth')->default('false');
            $table->boolean('dstar')->default('false');
            $table->boolean('sdr')->default('false');
            $table->jsonb('frequencies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('radios');
    }
}
