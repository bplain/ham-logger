<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmateursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amateurs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('called_by'); // Set by user. Default first name
            $table->date('grant_date');
            $table->date('expired_date')->nullable(); // follow previous_callsign in AM until the end
            $table->char('class',1);
            $table->bigInteger('frn')->unique();
            $table->string('email')->nullable();
            $table->string('street_address',60)->nullable();
            $table->string('city',20)->nullable();
            $table->char('state',2)->nullable();
            $table->integer('zip_code')->nullable();
            $table->integer('po_box')->nullable();
            $table->string('attention_line',35)->nullable();
            $table->char('country',3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amateurs');
    }
}
