<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->id();
            $table->foreignId('form_type_id');
            $table->jsonb('data');
            $table->foreignId('owner_id');
            $table->morphs('target_group'); // net, club, user
            $table->timestamps();
        });
        Schema::create('form_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->foreignId('form_category_id');
            $table->jsonb("config"); // JSON that describes each parameter
            $table->morphs('target_group'); // net, club, user
            $table->timestamps();
        });
        Schema::create('form_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        Schema::create('selected_form_types', function (Blueprint $table) {
            $table->foreignId('form_type_id');
            $table->morphs('target_group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
        Schema::dropIfExists('form_types');
    }
}
